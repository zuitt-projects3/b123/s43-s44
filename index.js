let coursesDiv = document.querySelector("#courses-div");
console.log(coursesDiv);
// console.log(coursesDiv.innerHTML);
//
// let mainDiv = document.querySelector("#main-div");
// console.log(typeof mainDiv.innerHTML)
//
// mainDiv.innerHTML = "<h1>Batch 123 is Awesomee!</h1>"
// mainDiv.innerHTML += "<p>Full Stack Developers</p>"
// mainDiv.innerHTML = mainDiv.innerHTML + "<p>Prince</p>"



// fetch() is a javascript method which allows us to pass or create a request to an api.
// syntax: fetch(<requestURL>)
// .then() allows us to handle/process result of a previous function
// .then(res => res.json()) - it handles/process the server's response and turns the response into a proper JS object
// .then(data => {}) - result of the res.json() processing your response and getting the proper data we asked from our server.
fetch('http://localhost:4000/courses/getActiveCourses')
.then(res => res.json())
.then(data => {

  // console.log(data);
  let courseCards = "";
  data.forEach((course)=> {
    console.log(course);
    // add a div for each item in our array

    courseCards += `
      <div class="card">
        <h4>${course.name}</h4>
        <p>${course.description}</p>
        <span>Price: ${course.price}</span>
      </div>

    `
  })
  console.log(courseCards);
  coursesDiv.innerHTML = courseCards;
})
