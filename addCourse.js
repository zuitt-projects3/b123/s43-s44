let nameInput = document.querySelector("#name-input");
let descriptionInput = document.querySelector("#description-input");
let priceInput = document.querySelector("#price-input");

let token = localStorage.getItem('token');
document.querySelector("#form-addCourse").addEventListener('submit',(e)=>{
  e.preventDefault();
  console.log(nameInput.value);
  console.log(descriptionInput.value);
  console.log(priceInput.value);

  fetch('http://localhost:4000/courses',{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify({
      name: nameInput.value,
      description: descriptionInput.value,
      price: priceInput.value
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
  })
})
