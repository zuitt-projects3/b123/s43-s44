// get user details

// Syntax: localStorage.getItem(<token>) - this will return the value of the key from our localStorage

let token = localStorage.getItem('token');
// console.log(token);
let profileName = document.querySelector("#profile-name")
let profileEmail = document.querySelector("#profile-email")
let profileMobile = document.querySelector("#profile-mobile")
fetch('http://localhost:4000/users/getUserDetails',{
  headers: {
    'Authorization' : `Bearer ${token}`
  }
})
.then(res => res.json())
.then(data => {

    profileName.innerHTML = `Welcome ${data.firstName} ${data.lastName}`
    profileEmail.innerHTML = data.email;
    profileMobile.innerHTML = data.mobileNo;

  })
